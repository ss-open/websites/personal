# Personal Website

## Run locally

### Build

```sh
docker compose build
```

### Execute

To get all the services up and running:

```sh
docker compose up
```

#### Application

```sh
docker compose run --rm --service-ports app
```

#### Storybook

```sh
docker compose run --rm --service-ports storybook
```
