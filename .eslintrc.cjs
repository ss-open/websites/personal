module.exports = {
  root: true,
  env: { browser: true, es2020: true },
  extends: [
    'eslint:recommended',
    'plugin:@typescript-eslint/strict-type-checked',
    'plugin:@typescript-eslint/stylistic-type-checked',
    'plugin:react-hooks/recommended',
    'plugin:storybook/recommended',
    'standard',
    'airbnb',
    'airbnb/hooks',
    'airbnb-typescript',
    'plugin:react/jsx-runtime',
  ],
  ignorePatterns: ['dist'],
  parser: '@typescript-eslint/parser',
  parserOptions: {
    project: [
      "tsconfig.json",
    ]
  },
  plugins: [
    'react-refresh',
    'import-newlines',
  ],
  rules: {
    'max-len': ["error", {
      code: 120,
    }],
    'import-newlines/enforce': [
      'error',
      {
        items: 0,
      }
    ],
    'import/no-absolute-path': 'off',
    'import/no-extraneous-dependencies': ['error', {
      devDependencies: [
        '**/*.test.{ts,tsx}',
        '**/.eslintrc.cjs',
        '**/vite.config.ts',
        '**/vitest.setup.ts',
        '.storybook/**',
      ],
    }],
    'import/prefer-default-export': 'off',
    'react/function-component-definition': ['error', {
      namedComponents: 'arrow-function',
      unnamedComponents: 'arrow-function',
    }],
    'react-refresh/only-export-components': [
      'warn',
      { allowConstantExport: true },
    ],
  },
}
