FROM oven/bun:1.1.21-alpine as base
# Needed for some binaries that needs nodejs to work properly
# which is respected by `bun run` and `bunx` without the `--bun` cli option.
# @see https://bun.sh/docs/cli/bunx#shebangs
RUN apk add --no-cache nodejs~=18
WORKDIR /app
COPY package.json .
COPY bun.lockb .
RUN bun install
COPY public public
COPY src src
COPY index.html .
COPY vite.config.ts .
COPY tsconfig.json .
COPY tsconfig.node.json .

FROM base as app
CMD ["bun", "run", "dev"]

FROM base as test
COPY vitest.setup.ts .
CMD ["bun", "run", "coverage"]

FROM base as chromatic
RUN apk add --no-cache git~=2
COPY .storybook .storybook
CMD ["bun", "run", "chromatic"]
