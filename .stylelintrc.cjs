module.exports = {
  plugins: [
    "stylelint-value-no-unknown-custom-properties"
  ],
  extends: ['stylelint-config-standard'],
  rules: {
    // We SHOULD move to the no-unknown-custom-properties official rules instead
    // once a global css file definition will be available.
    // @see https://github.com/stylelint/stylelint/issues/6746
    "csstools/value-no-unknown-custom-properties": [true, {
      "importFrom": [
        "src/index.css",
      ]
    }]
  },
};
