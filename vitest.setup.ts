import {
  afterEach,
} from 'vitest';
import {
  cleanup,
} from '@testing-library/react';

afterEach(() => {
  cleanup();
  // Ensures storages are emptied after each test in order to
  // avoid side effects between them because of residual data
  window.sessionStorage.clear();
  window.localStorage.clear();
});
