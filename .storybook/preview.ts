import type {
  Preview,
} from '@storybook/react';
import {
  themes,
} from '@storybook/theming';
import {
  withThemeDecorator,
} from './components';
import '../src/index.css';

const preview: Preview = {
  parameters: {
    actions: { argTypesRegex: '^on[A-Z].*' },
    controls: {
      matchers: {
        color: /(background|color)$/i,
        date: /Date$/i,
      },
    },
    docs: {
      toc: true,
      theme: themes.dark,
    },
  },
  decorators: [
    withThemeDecorator({
      defaultTheme: 'system',
    }),
  ],
};

export default preview;
