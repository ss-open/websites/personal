import {
  useEffect,
} from 'react';
import {
  Decorator,
} from '@storybook/react';
import {
  DecoratorHelpers,
} from '@storybook/addon-themes';
import type {
  TernaryDarkMode,
} from 'usehooks-ts';
import {
  useThemeSwitch,
} from '../../src/hooks';

const {
  initializeThemeState,
  pluckThemeFromContext,
} = DecoratorHelpers;

const themes: TernaryDarkMode[] = [
  'dark',
  'light',
  'system',
];

export interface WithThemeDecoratorStrategyConfiguration {
  defaultTheme: TernaryDarkMode,
}

export const withThemeDecorator = ({
  defaultTheme,
}: WithThemeDecoratorStrategyConfiguration): Decorator => {
  initializeThemeState(themes, defaultTheme);

  return (Story, context) => {
    const {
      setTernaryDarkMode,
    } = useThemeSwitch();
    const selectedTheme = pluckThemeFromContext(context);
    const theme = selectedTheme || defaultTheme;

    useEffect(() => {
      setTernaryDarkMode(theme as TernaryDarkMode);
    }, [
      theme,
      setTernaryDarkMode,
    ]);

    return <Story />;
  };
};

export default null;
