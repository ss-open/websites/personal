/// <reference types="vitest" />
import {
  defineConfig,
} from 'vite';
import react from '@vitejs/plugin-react';

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react()],
  test: {
    environment: 'happy-dom',
    reporters: ['junit', 'default'],
    outputFile: {
      junit: './test-report/junit.xml',
    },
    coverage: {
      // Needed to avoid conflict with the docker compose volume binding on CI.
      clean: false,
      provider: 'istanbul',
      reporter: [
        'text-summary',
        'text',
        'html',
      ],
      include: [
        'src/**/*.@(ts|tsx)',
      ],
      exclude: [
        '**/*.stories.tsx',
      ],
    },
    setupFiles: [
      './vitest.setup.ts',
    ],
  },
});
