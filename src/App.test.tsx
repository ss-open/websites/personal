import {
  expect,
  it,
} from 'vitest';
import {
  composeStory,
} from '@storybook/react';
import {
  render,
  screen,
} from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import '@testing-library/jest-dom/vitest';
import Meta, {
  Default as DefaultStory,
} from './App.stories';

const App = composeStory(DefaultStory, Meta);

it('renders', async () => {
  render(<App />);

  expect(screen.getByText('Click on the Vite and React logos to learn more')).toBeInTheDocument();
});

it('counts on click', async () => {
  const user = userEvent.setup();
  render(<App />);

  const countButton = screen.getByRole('button', {
    name: /^count is \d+$/,
  });
  expect(countButton).toBeInTheDocument();
  expect(countButton.textContent).toBe('count is 0');
  await user.click(countButton);
  expect(countButton.textContent).toBe('count is 1');
});
