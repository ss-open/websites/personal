/* eslint-disable @typescript-eslint/no-non-null-assertion */
import React from 'react';
import ReactDOM from 'react-dom/client';
import {
  App,
} from './App';
import './index.css';

// @see https://stackoverflow.com/q/77805783/1731473
/* istanbul ignore next -- @preserve */
ReactDOM.createRoot(document.getElementById('root')!).render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
);
