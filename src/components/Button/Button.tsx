import {
  ButtonHTMLAttributes,
  FC,
} from 'react';

export type ButtonProps = ButtonHTMLAttributes<HTMLButtonElement>;

// eslint-disable-next-line react/jsx-props-no-spreading
export const Button: FC<ButtonProps> = (props) => <button type="button" {...props} />;
