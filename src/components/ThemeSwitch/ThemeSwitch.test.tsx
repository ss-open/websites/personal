import {
  expect,
  it,
} from 'vitest';
import {
  composeStory,
} from '@storybook/react';
import {
  render,
  screen,
} from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import '@testing-library/jest-dom/vitest';
import Meta, {
  Default as DefaultStory,
} from './ThemeSwitch.stories';

const ThemeSwitch = composeStory(DefaultStory, Meta);

it('selects a theme', async () => {
  const user = userEvent.setup();
  render(<ThemeSwitch />);

  const select = screen.getByRole<HTMLSelectElement>('combobox', {
    name: 'Theme select',
  });
  const themeSystem = screen.getByRole<HTMLOptionElement>('option', {
    name: 'System',
  });
  const themeLight = screen.getByRole<HTMLOptionElement>('option', {
    name: 'Light',
  });
  const themeDark = screen.getByRole<HTMLOptionElement>('option', {
    name: 'Dark',
  });
  expect(select).toBeInTheDocument();
  expect(select).toBeVisible();
  expect(select.value).toBe('system');
  expect(themeSystem.selected).toBe(true);
  expect(themeLight.selected).toBe(false);
  expect(themeDark.selected).toBe(false);
  await user.selectOptions(select, 'light');
  expect(select.value).toBe('light');
  expect(themeSystem.selected).toBe(false);
  expect(themeLight.selected).toBe(true);
});
