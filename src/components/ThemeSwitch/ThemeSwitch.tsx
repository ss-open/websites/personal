import {
  FC,
} from 'react';
import type {
  TernaryDarkMode,
} from 'usehooks-ts';
import {
  useThemeSwitch,
} from '../../hooks';

export const ThemeSwitch: FC = () => {
  const {
    ternaryDarkMode,
    setTernaryDarkMode,
  } = useThemeSwitch();

  return (
    <select
      name="theme"
      aria-label="Theme select"
      onChange={(ev) => {
        setTernaryDarkMode(ev.target.value as TernaryDarkMode);
      }}
      value={ternaryDarkMode}
    >
      <option value="system">System</option>
      <option value="light">Light</option>
      <option value="dark">Dark</option>
    </select>
  );
};
