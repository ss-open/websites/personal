import {
  useEffect,
} from 'react';
import {
  TernaryDarkModeReturn,
  useMediaQuery,
  useTernaryDarkMode,
} from 'usehooks-ts';

type UseThemeSwitchReturnType =
TernaryDarkModeReturn
& {
  isSystemDarkMode: boolean;
};

// @see https://stackoverflow.com/a/76795904/1731473
export const useThemeSwitch = (): UseThemeSwitchReturnType => {
  const isSystemDarkMode = useMediaQuery('(prefers-color-scheme: dark)');
  const {
    isDarkMode,
    ternaryDarkMode,
    ...ternaryDarkModeRest
  } = useTernaryDarkMode();

  useEffect(() => {
    for (let s = 0; s < document.styleSheets.length; s += 1) {
      const styleSheet = document.styleSheets.item(s);

      if (typeof styleSheet?.cssRules !== 'undefined') {
        for (let r = 0; r < styleSheet.cssRules.length; r += 1) {
          const cssRule = styleSheet.cssRules.item(r);

          if (cssRule instanceof CSSMediaRule) {
            const ruleMediaText = cssRule.media.mediaText;

            if (ruleMediaText.match(/(prefers-color-scheme: \w+)/)) {
              /**
               * Replace the prefers-color-scheme media query to apply the desired theme.
               * It refers to the system media query in order to keep the mechanic in sync by reversing it.
               */
              const newRuleMediaText = ruleMediaText.replace(
                /(dark|light)/,
                // eslint-disable-next-line no-nested-ternary
                isDarkMode
                  ? isSystemDarkMode ? 'dark' : 'light'
                  : isSystemDarkMode ? 'light' : 'dark',
              );

              cssRule.media.deleteMedium(ruleMediaText);
              cssRule.media.appendMedium(newRuleMediaText);
            }
          }
        }
      }
    }
  }, [isDarkMode, isSystemDarkMode]);

  return {
    isSystemDarkMode,
    isDarkMode,
    ternaryDarkMode,
    ...ternaryDarkModeRest,
  };
};
