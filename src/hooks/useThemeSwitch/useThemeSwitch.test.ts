import {
  beforeEach,
  expect,
  it,
  vi,
  describe,
} from 'vitest';
import {
  act,
  renderHook,
} from '@testing-library/react';
import '@testing-library/jest-dom/vitest';
import {
  useThemeSwitch,
} from './useThemeSwitch';

describe('tests', () => {
  let styleSheetList: StyleSheetList;

  beforeEach(() => {
    const cssPreferColorSchemeMediaRule = new CSSMediaRule();
    cssPreferColorSchemeMediaRule.media.appendMedium('(prefers-color-scheme: light)');
    const cssOtherMediaRule = new CSSMediaRule();
    cssOtherMediaRule.media.appendMedium('(min-width: 1080px)');
    const cssRule = new CSSStyleRule();

    const cssRules: CSSRule[] = [
      cssPreferColorSchemeMediaRule,
      cssOtherMediaRule,
      cssRule,
    ];
    const cssRuleList: CSSRuleList = {
      [Symbol.iterator]: () => cssRules.values(),
      length: cssRules.length,
      item: (c) => cssRules[c],
    };
    const cssStyleSheet: CSSStyleSheet = {
      ...new CSSStyleSheet(),
      cssRules: cssRuleList,
    };
    const styleSheets = [cssStyleSheet];
    styleSheetList = {
      [Symbol.iterator]: () => styleSheets.values(),
      length: styleSheets.length,
      item: (s) => styleSheets[s],
    };

    Object.defineProperty(document, 'styleSheets', {
      value: styleSheetList,
      configurable: true,
    });
  });

  it('switches theme', () => {
    const { result } = renderHook(useThemeSwitch);

    expect(result.current.isDarkMode).toBe(false);
    // @ts-expect-error testing purpose
    // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
    expect(styleSheetList.item(0)?.cssRules.item(0)?.media[0]).toBe('(prefers-color-scheme: dark)');
    act(() => {
      result.current.setTernaryDarkMode('dark');
    });
    expect(result.current.isDarkMode).toBe(true);
    // @ts-expect-error testing purpose
    // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
    expect(styleSheetList.item(0)?.cssRules.item(0)?.media[0]).toBe('(prefers-color-scheme: light)');
  });

  it('switches theme with system dark mode enabled', () => {
    const matchMediaOriginal = window.matchMedia;
    window.matchMedia = vi.fn().mockImplementation((query: string): MediaQueryList => {
      const mediaQueryList: MediaQueryList = {
        matches: false,
        media: query,
        onchange: null,
        addListener: vi.fn(),
        removeListener: vi.fn(),
        addEventListener: vi.fn(),
        removeEventListener: vi.fn(),
        dispatchEvent: vi.fn(),
      };

      if (query === '(prefers-color-scheme: dark)') {
        return {
          ...mediaQueryList,
          matches: true,
        };
      }

      return mediaQueryList;
    });

    const { result } = renderHook(() => useThemeSwitch());

    expect(result.current.isDarkMode).toBe(true);
    /* eslint-disable @typescript-eslint/no-unsafe-member-access */
    // @ts-expect-error testing purpose
    expect(styleSheetList.item(0)?.cssRules.item(0)?.media[0]).toBe('(prefers-color-scheme: dark)');
    act(() => {
      result.current.setTernaryDarkMode('light');
    });
    expect(result.current.isDarkMode).toBe(false);
    // @ts-expect-error testing purpose
    expect(styleSheetList.item(0)?.cssRules.item(0)?.media[0]).toBe('(prefers-color-scheme: light)');
    /* eslint-enable @typescript-eslint/no-unsafe-member-access */

    window.matchMedia = matchMediaOriginal;
  });
});

describe('edge cases', () => {
  // Handles a case that SHOULD not happen because we SHOULD recieve a correct stylesheets
  // definition with a length that is already handle by the for loop of the hooks.
  // In other words, the related document.styleSheets.item(s) SHOULD NEVER return null,
  // making the cssRules property undefined.
  it('handles no css rules', () => {
    const styleSheets = [new CSSStyleSheet()];
    const styleSheetList = {
      [Symbol.iterator]: () => styleSheets.values(),
      length: styleSheets.length,
      item: () => null,
    };

    Object.defineProperty(document, 'styleSheets', {
      value: styleSheetList,
      configurable: true,
    });

    const { result } = renderHook(useThemeSwitch);

    expect(result.current.isDarkMode).toBe(false);
  });
});
