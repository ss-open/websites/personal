/* eslint-disable @typescript-eslint/no-unsafe-member-access */
import type {
  Meta,
  StoryFn,
} from '@storybook/react';
import {
  assert,
  describe,
  expect,
  test,
} from 'vitest';
import {
  render,
} from '@testing-library/react';
import {
  composeStories,
} from '@storybook/react';
import path from 'path';
import {
  globSync,
} from 'glob';

interface StoryFile {
  default: Meta;
  [name: string]: StoryFn | Meta;
}

const compose = (entry: StoryFile): ReturnType<typeof composeStories<StoryFile>> => {
  try {
    return composeStories(entry);
  } catch (e) {
    throw new Error(
      // eslint-disable-next-line @typescript-eslint/restrict-template-expressions
      `There was an issue composing stories for the module: ${JSON.stringify(entry)}, ${e}`,
    );
  }
};

function getAllStoryFiles() {
  const storyFiles = Object.entries(
    import.meta.glob<StoryFile>('./**/*.stories.@(js|jsx|mjs|ts|tsx)', {
      eager: true,
    }),
  );

  return storyFiles.map(([filePath, storyFile]) => {
    const storyDir = path.dirname(filePath);
    const componentName = path.basename(filePath).replace(/\.(stories|story)\.[^/.]+$/, '');
    return {
      filePath, storyFile, componentName, storyDir,
    };
  });
}

// Recreate similar options to Storyshots. Place your configuration below
const options = {
  suite: 'Storyshots',
  snapshotsDirName: '__snapshots__',
  snapshotExtension: '.snap.html',
};

describe(options.suite, () => {
  getAllStoryFiles().forEach(({ storyFile, componentName, storyDir }) => {
    const meta = storyFile.default;
    const title = meta.title ?? componentName;

    if (meta.parameters?.storyshots?.disable) {
      return;
    }

    describe(title, () => {
      const stories = Object.entries(compose(storyFile))
        .map(([name, story]) => ({ name, story }))
        .filter(({ story }) => !story.parameters.storyshots?.disable);

      if (stories.length <= 0) {
        throw new Error(
          `No stories found for this module: ${title}.`
          + 'Make sure there is at least one valid story for this module,'
          + 'without a disable parameter, or add parameters.storyshots.disable in the default export of this file.',
        );
      }

      stories.forEach(({ name, story }) => {
        const testFn = story.parameters.storyshots?.skip ? test.skip : test;

        testFn(name, async () => {
          const mounted = render(story());
          // eslint-disable-next-line no-promise-executor-return
          await new Promise((resolve) => setTimeout(resolve, 1));

          const snapshotPath = path.join(
            storyDir,
            options.snapshotsDirName,
            `${componentName}.${name}${options.snapshotExtension}`,
          );
          await expect(mounted.container).toMatchFileSnapshot(snapshotPath);
        });
      });

      const snapshotFileNames = globSync(
        `${componentName}.*${options.snapshotExtension}`,
        {
          cwd: path.join(
            __dirname,
            storyDir,
            options.snapshotsDirName,
          ),
        },
      )
        .map((snapshotFileName) => ({
          fileName: snapshotFileName,
          storyName: snapshotFileName.split('.')[1],
        }));

      const snapshotLegacyFileNames = snapshotFileNames.filter(({
        storyName,
      }) => stories.findIndex(({ name }) => name === storyName) === -1);

      snapshotLegacyFileNames.forEach(({ storyName }) => {
        test(storyName, () => {
          assert.fail('This snapshot is legacy and should be removed.');
        });
      });
    });
  });
});
